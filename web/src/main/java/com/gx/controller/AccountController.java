package com.gx.controller;

import com.gx.domain.Account;
import com.gx.domain.Alltable;
import com.gx.domain.patientdb;
import com.gx.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AccountController {

    @Autowired
    private AccountService accountService;

    @RequestMapping("/account/findAll")
    public String findAll(Model model) {  //存数据， Model对象
        System.out.println("Controller表现层：查询...");
        // 调用service的方法
        List<Alltable> list = accountService.findAll();
        List categoryid =new ArrayList();
        List clickCount =new ArrayList();
        for (Alltable a:list
        ) {
            categoryid.add(a.getCategoryid());
            clickCount.add(a.getClickCount());
            System.out.println(clickCount);
        }
        model.addAttribute("categoryid", categoryid);
        model.addAttribute("clickCount", clickCount);
        System.out.println("跳转list页面");
        return "WEB-INF/list";
    }
    @RequestMapping("/account/top10_category")
    public String top10_category(Model model) {  //存数据， Model对象
        System.out.println("Controller表现层：查询...");
        // 调用service的方法
        List<Alltable> list = accountService.top10_category();
        List sessionid =new ArrayList();
        List Categoryid =new ArrayList();
        for (Alltable a:list
        ) {
            sessionid.add(a.getClickCount());
            Categoryid.add(a.getCategoryid());
            System.out.println(Categoryid);
        }
        model.addAttribute("clickCount", sessionid);
        model.addAttribute("Categoryid", Categoryid);
        System.out.println("跳转list页面");
        return "WEB-INF/top10Categoryid";
    }
    @RequestMapping("/account/ad_province_top3")
    public String ad_province_top3(Model model) {  //存数据， Model对象
        System.out.println("Controller表现层：查询...");
        // 调用service的方法
        List<Alltable> list = accountService.ad_province_top3();
        List province =new ArrayList();
        List clickCount =new ArrayList();
        ArrayList<String> adid=new ArrayList<String>();
        for (Alltable a:list
        ) {
            province.add(a.getProvince());
            clickCount.add(a.getClickCount());
            adid.add(a.getIds());
            System.out.println(adid);
        }
        model.addAttribute("province", province);
        model.addAttribute("clickCount", clickCount);
        model.addAttribute("ids", adid);
        System.out.println("跳转list页面");
        return "WEB-INF/ad_province_top3";
    }

    @RequestMapping("/account/convertRate")
    public String convertRate(Model model) {  //存数据， Model对象
        System.out.println("Controller表现层：查询...");
        // 调用service的方法
        String list = accountService.convertRate();
        List str1=new ArrayList();
        String[] str2=list.split("\\|");
        String[] str3=new String[str2.length-1];
        for (String x:str2
        ) {
            if(x.contains("1_2"))
                str3[0]=x.substring(4);
            if(x.contains("2_3"))
                str3[1]=x.substring(4);
            if(x.contains("3_4"))
                str3[2]=x.substring(4);
            if(x.contains("4_5"))
                str3[3]=x.substring(4);
            if(x.contains("5_6"))
                str3[4]=x.substring(4);
        }
        double[] x3=new double[str2.length-1];

        int i=0;
        for (String x:str3
        ) {
            x3[i]=Double.parseDouble(x);
            str1.add(x3[i]);
            i++;
        }
        model.addAttribute("convertRate", str1);
        System.out.println("跳转页面");
        return "WEB-INF/convertRate";
    }

    @RequestMapping("/account/ad_user_click_count")
    public String ad_user_click_count(Model model) {  //存数据， Model对象
        System.out.println("Controller表现层：查询...");
        // 调用service的方法
        List<Alltable> list = accountService.ad_user_click_count();
        List userid =new ArrayList();
        List clickCount =new ArrayList();
        ArrayList<String> adid=new ArrayList<String>();
        for (Alltable a:list
        ) {
            userid.add(a.getUserid());
            clickCount.add(a.getClickCount());
            System.out.println(userid);
        }
        model.addAttribute("userid", userid);
        model.addAttribute("clickCount", clickCount);
        System.out.println("跳转页面");
        return "WEB-INF/ad_user_click_count";
    }

    @RequestMapping("/account/save")
    public void save(Account account, HttpServletRequest request, HttpServletResponse response) throws IOException {
        accountService.saveAccount(account);
        response.sendRedirect(request.getContextPath() + "/account/findAll");
    }
}
