package com.gx.service;

import com.gx.domain.Account;
import com.gx.domain.Alltable;
import com.gx.domain.patientdb;

import java.util.List;

public interface AccountService {
    // 查询所有账户
    public List<Alltable> findAll();

    // 保存帐户信息
    public void saveAccount(Account account);

    List<Alltable> top10_category();

    List<Alltable> ad_province_top3();

    List<Alltable> area_product_top3();

    String convertRate();

    List<Alltable> ad_user_click_count();
}
