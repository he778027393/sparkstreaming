package com.gx.service.Impl;

import com.gx.dto.IAccountdto;
import com.gx.domain.Account;
import com.gx.domain.Alltable;
import com.gx.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Autowired
    private IAccountdto iaccountdao;

    @Override
    public List<Alltable> findAll() {
        System.out.println("Service业务层：查询所有账户...");
        return iaccountdao.findTop10Session();
    }

    @Override
    public void saveAccount(Account account) {
        System.out.println("Service业务层：保存帐户...");
        iaccountdao.saveAccount(account);
    }

    @Override
    public List<Alltable> top10_category() {
        return iaccountdao.top10_category();
    }

    @Override
    public List<Alltable> ad_province_top3() {
        return iaccountdao.ad_province_top3();
    }

    @Override
    public List<Alltable> area_product_top3() {
        return iaccountdao.area_product_top3();
    }
    @Override
    public String convertRate() {
        return iaccountdao.convertRate();
    }

    @Override
    public List<Alltable> ad_user_click_count() {
        return iaccountdao.ad_user_click_count();
    }
}
