package com.gx.dto;

import com.gx.domain.Account;
import com.gx.domain.Alltable;
import com.gx.domain.patientdb;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IAccountdto {
/*基于mybatis的注解开发，使用的是注解，如果看不懂再温顾mybatis吧~*/
    @Select("select * from patient_aggr_stat where taskid=1")
    public patientdb findAll();
    @Insert("insert into tab_user (uid,username) value(#{uid},#{username})")
    public void saveAccount(Account account);
    @Select("select categoryid,sum(clickCount) as clickCount from top10_session_0308 GROUP BY categoryid order by clickCount desc limit 10")
    public List<Alltable> findTop10Session();
    @Select("select categoryid,clickCount+payCount+orderCount as clickCount from top10_category_0308 order by clickCount desc limit 10")
    public  List<Alltable> top10_category();
    @Select("SELECT province,GROUP_CONCAT(adid) ids,sum(clickCount) as clickCount from ad_province_top3 GROUP BY province ORDER BY clickCount desc limit 10")
    public  List<Alltable> ad_province_top3();
    @Select("SELECT area,GROUP_CONCAT(productName) as nameConcat from area_top3_product_0308 GROUP BY area limit 10")
    public  List<Alltable> area_product_top3();
    @Select("SELECT convertRate from page_split_convert_rate_0308")
    public  String convertRate();
    @Select("SELECT userid,sum(clickCount) as clickCount from ad_user_click_count GROUP BY userid ORDER BY userid")
    public List<Alltable> ad_user_click_count();


}
