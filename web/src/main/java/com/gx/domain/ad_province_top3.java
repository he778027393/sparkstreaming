package com.gx.domain;

public class ad_province_top3 {
    private String data;
    private String province;
    private int adid;
    private int clickCount;

    @Override
    public String toString() {
        return "ad_province_top3{" +
                "data='" + data + '\'' +
                ", province='" + province + '\'' +
                ", adid=" + adid +
                ", clickCount=" + clickCount +
                '}';
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int getAdid() {
        return adid;
    }

    public void setAdid(int adid) {
        this.adid = adid;
    }

    public int getClickCount() {
        return clickCount;
    }

    public void setClickCount(int clickCount) {
        this.clickCount = clickCount;
    }
}
