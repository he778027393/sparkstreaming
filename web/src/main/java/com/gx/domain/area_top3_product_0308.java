package com.gx.domain;

public class area_top3_product_0308 {
    private String taskid;
    private String area;
    private String areaLevel;
    private int productid;
    private String cityLnfos;

    @Override
    public String toString() {
        return "area_top3_product_0308{" +
                "taskid='" + taskid + '\'' +
                ", area='" + area + '\'' +
                ", areaLevel='" + areaLevel + '\'' +
                ", productid=" + productid +
                ", cityLnfos='" + cityLnfos + '\'' +
                '}';
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaLevel() {
        return areaLevel;
    }

    public void setAreaLevel(String areaLevel) {
        this.areaLevel = areaLevel;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public String getCityLnfos() {
        return cityLnfos;
    }

    public void setCityLnfos(String cityLnfos) {
        this.cityLnfos = cityLnfos;
    }
}
