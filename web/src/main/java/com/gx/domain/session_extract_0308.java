package com.gx.domain;

public class session_extract_0308 {
    private String taskid;
    private String sessionid;
    private String startTime;
    private String searchKeywords;
    private String clickCategoryLds;

    @Override
    public String toString() {
        return "session_extract_0308{" +
                "taskid='" + taskid + '\'' +
                ", sessionid='" + sessionid + '\'' +
                ", startTime='" + startTime + '\'' +
                ", searchKeywords='" + searchKeywords + '\'' +
                ", clickCategoryLds='" + clickCategoryLds + '\'' +
                '}';
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getSearchKeywords() {
        return searchKeywords;
    }

    public void setSearchKeywords(String searchKeywords) {
        this.searchKeywords = searchKeywords;
    }

    public String getClickCategoryLds() {
        return clickCategoryLds;
    }

    public void setClickCategoryLds(String clickCategoryLds) {
        this.clickCategoryLds = clickCategoryLds;
    }
}
