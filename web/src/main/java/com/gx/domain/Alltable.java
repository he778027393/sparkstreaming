package com.gx.domain;

public class Alltable {
    private String ids;

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    //top10session
    private String taskid;
    private int categoryid;
    private String sessionid;
    private int clickCount;
    //top10category
    private int orderCount;
    private int payCount;
    //session_random_extract
    private String startTime;
    private String searchKeywords;
    private String clickCategoryIds;
    //sesson_detail
    private int pageid;
    private String actonTime;
    private String searchKeyword;
    private int clickProductId;
    private int clickProductIds;
    private String oderCategoryIds;
    private String oderProductIds;
    private String payCategoryIds;
    private String payProductIds;
    //session_aggr_stat
    private int session_count;
    private double visit_length_1s_3s_ratio;
    private double visit_length_4s_6s_ratio;
    private double visit_length_7s_9s_ratio;
    private double visit_length_10s_30s_ratio;
    private double visit_length_30s_60s_ratio;
    private double visit_length_1m_3m_ratio;
    private double visit_length_3m_10m_ratio;
    private double visit_length_10m_30m_ratio;
    private double visit_length_30m_ratio;
    private double step_length_1_3_ratio;
    private double step_length_4_6_ratio;
    private double step_length_7_9_ratio;
    private double step_length_10_30_ratio;
    private double step_length_30_60_ratio;
    private double step_length_60_ratio;
    //page_split_convert_rate
    private String converRate;
    //area_top3_product
    private String area;
    private String areaLevel;
    private int productid;
    private String cityLnfos;
    private String productName;
    private String productStatus;
    //ad_user_click_count
    private String data;
    //ad_blacklist
    private int userid;
    private int adid;
    //ad_stat  ad_province_top3
    private String province;
    private String city;
    private String hour;
    private String minute;

    @Override
    public String toString() {
        return "Alltable{" +
                "taskid='" + taskid + '\'' +
                ", categoryid=" + categoryid +
                ", sessionid='" + sessionid + '\'' +
                ", clickCount=" + clickCount +
                ", orderCount=" + orderCount +
                ", payCount=" + payCount +
                ", startTime='" + startTime + '\'' +
                ", searchKeywords='" + searchKeywords + '\'' +
                ", clickCategoryIds='" + clickCategoryIds + '\'' +
                ", pageid=" + pageid +
                ", actonTime='" + actonTime + '\'' +
                ", searchKeyword='" + searchKeyword + '\'' +
                ", clickProductId=" + clickProductId +
                ", clickProductIds=" + clickProductIds +
                ", oderCategoryIds='" + oderCategoryIds + '\'' +
                ", oderProductIds='" + oderProductIds + '\'' +
                ", payCategoryIds='" + payCategoryIds + '\'' +
                ", payProductIds='" + payProductIds + '\'' +
                ", session_count=" + session_count +
                ", visit_length_1s_3s_ratio=" + visit_length_1s_3s_ratio +
                ", visit_length_4s_6s_ratio=" + visit_length_4s_6s_ratio +
                ", visit_length_7s_9s_ratio=" + visit_length_7s_9s_ratio +
                ", visit_length_10s_30s_ratio=" + visit_length_10s_30s_ratio +
                ", visit_length_30s_60s_ratio=" + visit_length_30s_60s_ratio +
                ", visit_length_1m_3m_ratio=" + visit_length_1m_3m_ratio +
                ", visit_length_3m_10m_ratio=" + visit_length_3m_10m_ratio +
                ", visit_length_10m_30m_ratio=" + visit_length_10m_30m_ratio +
                ", visit_length_30m_ratio=" + visit_length_30m_ratio +
                ", step_length_1_3_ratio=" + step_length_1_3_ratio +
                ", step_length_4_6_ratio=" + step_length_4_6_ratio +
                ", step_length_7_9_ratio=" + step_length_7_9_ratio +
                ", step_length_10_30_ratio=" + step_length_10_30_ratio +
                ", step_length_30_60_ratio=" + step_length_30_60_ratio +
                ", step_length_60_ratio=" + step_length_60_ratio +
                ", converRate='" + converRate + '\'' +
                ", area='" + area + '\'' +
                ", areaLevel='" + areaLevel + '\'' +
                ", productid=" + productid +
                ", cityLnfos='" + cityLnfos + '\'' +
                ", productName='" + productName + '\'' +
                ", productStatus='" + productStatus + '\'' +
                ", data='" + data + '\'' +
                ", userid=" + userid +
                ", adid=" + adid +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", hour='" + hour + '\'' +
                ", minute='" + minute + '\'' +
                '}';
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public int getClickCount() {
        return clickCount;
    }

    public void setClickCount(int clickCount) {
        this.clickCount = clickCount;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public int getPayCount() {
        return payCount;
    }

    public void setPayCount(int payCount) {
        this.payCount = payCount;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getSearchKeywords() {
        return searchKeywords;
    }

    public void setSearchKeywords(String searchKeywords) {
        this.searchKeywords = searchKeywords;
    }

    public String getClickCategoryIds() {
        return clickCategoryIds;
    }

    public void setClickCategoryIds(String clickCategoryIds) {
        this.clickCategoryIds = clickCategoryIds;
    }

    public int getPageid() {
        return pageid;
    }

    public void setPageid(int pageid) {
        this.pageid = pageid;
    }

    public String getActonTime() {
        return actonTime;
    }

    public void setActonTime(String actonTime) {
        this.actonTime = actonTime;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public int getClickProductId() {
        return clickProductId;
    }

    public void setClickProductId(int clickProductId) {
        this.clickProductId = clickProductId;
    }

    public int getClickProductIds() {
        return clickProductIds;
    }

    public void setClickProductIds(int clickProductIds) {
        this.clickProductIds = clickProductIds;
    }

    public String getOderCategoryIds() {
        return oderCategoryIds;
    }

    public void setOderCategoryIds(String oderCategoryIds) {
        this.oderCategoryIds = oderCategoryIds;
    }

    public String getOderProductIds() {
        return oderProductIds;
    }

    public void setOderProductIds(String oderProductIds) {
        this.oderProductIds = oderProductIds;
    }

    public String getPayCategoryIds() {
        return payCategoryIds;
    }

    public void setPayCategoryIds(String payCategoryIds) {
        this.payCategoryIds = payCategoryIds;
    }

    public String getPayProductIds() {
        return payProductIds;
    }

    public void setPayProductIds(String payProductIds) {
        this.payProductIds = payProductIds;
    }

    public int getSession_count() {
        return session_count;
    }

    public void setSession_count(int session_count) {
        this.session_count = session_count;
    }

    public double getVisit_length_1s_3s_ratio() {
        return visit_length_1s_3s_ratio;
    }

    public void setVisit_length_1s_3s_ratio(double visit_length_1s_3s_ratio) {
        this.visit_length_1s_3s_ratio = visit_length_1s_3s_ratio;
    }

    public double getVisit_length_4s_6s_ratio() {
        return visit_length_4s_6s_ratio;
    }

    public void setVisit_length_4s_6s_ratio(double visit_length_4s_6s_ratio) {
        this.visit_length_4s_6s_ratio = visit_length_4s_6s_ratio;
    }

    public double getVisit_length_7s_9s_ratio() {
        return visit_length_7s_9s_ratio;
    }

    public void setVisit_length_7s_9s_ratio(double visit_length_7s_9s_ratio) {
        this.visit_length_7s_9s_ratio = visit_length_7s_9s_ratio;
    }

    public double getVisit_length_10s_30s_ratio() {
        return visit_length_10s_30s_ratio;
    }

    public void setVisit_length_10s_30s_ratio(double visit_length_10s_30s_ratio) {
        this.visit_length_10s_30s_ratio = visit_length_10s_30s_ratio;
    }

    public double getVisit_length_30s_60s_ratio() {
        return visit_length_30s_60s_ratio;
    }

    public void setVisit_length_30s_60s_ratio(double visit_length_30s_60s_ratio) {
        this.visit_length_30s_60s_ratio = visit_length_30s_60s_ratio;
    }

    public double getVisit_length_1m_3m_ratio() {
        return visit_length_1m_3m_ratio;
    }

    public void setVisit_length_1m_3m_ratio(double visit_length_1m_3m_ratio) {
        this.visit_length_1m_3m_ratio = visit_length_1m_3m_ratio;
    }

    public double getVisit_length_3m_10m_ratio() {
        return visit_length_3m_10m_ratio;
    }

    public void setVisit_length_3m_10m_ratio(double visit_length_3m_10m_ratio) {
        this.visit_length_3m_10m_ratio = visit_length_3m_10m_ratio;
    }

    public double getVisit_length_10m_30m_ratio() {
        return visit_length_10m_30m_ratio;
    }

    public void setVisit_length_10m_30m_ratio(double visit_length_10m_30m_ratio) {
        this.visit_length_10m_30m_ratio = visit_length_10m_30m_ratio;
    }

    public double getVisit_length_30m_ratio() {
        return visit_length_30m_ratio;
    }

    public void setVisit_length_30m_ratio(double visit_length_30m_ratio) {
        this.visit_length_30m_ratio = visit_length_30m_ratio;
    }

    public double getStep_length_1_3_ratio() {
        return step_length_1_3_ratio;
    }

    public void setStep_length_1_3_ratio(double step_length_1_3_ratio) {
        this.step_length_1_3_ratio = step_length_1_3_ratio;
    }

    public double getStep_length_4_6_ratio() {
        return step_length_4_6_ratio;
    }

    public void setStep_length_4_6_ratio(double step_length_4_6_ratio) {
        this.step_length_4_6_ratio = step_length_4_6_ratio;
    }

    public double getStep_length_7_9_ratio() {
        return step_length_7_9_ratio;
    }

    public void setStep_length_7_9_ratio(double step_length_7_9_ratio) {
        this.step_length_7_9_ratio = step_length_7_9_ratio;
    }

    public double getStep_length_10_30_ratio() {
        return step_length_10_30_ratio;
    }

    public void setStep_length_10_30_ratio(double step_length_10_30_ratio) {
        this.step_length_10_30_ratio = step_length_10_30_ratio;
    }

    public double getStep_length_30_60_ratio() {
        return step_length_30_60_ratio;
    }

    public void setStep_length_30_60_ratio(double step_length_30_60_ratio) {
        this.step_length_30_60_ratio = step_length_30_60_ratio;
    }

    public double getStep_length_60_ratio() {
        return step_length_60_ratio;
    }

    public void setStep_length_60_ratio(double step_length_60_ratio) {
        this.step_length_60_ratio = step_length_60_ratio;
    }

    public String getConverRate() {
        return converRate;
    }

    public void setConverRate(String converRate) {
        this.converRate = converRate;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaLevel() {
        return areaLevel;
    }

    public void setAreaLevel(String areaLevel) {
        this.areaLevel = areaLevel;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public String getCityLnfos() {
        return cityLnfos;
    }

    public void setCityLnfos(String cityLnfos) {
        this.cityLnfos = cityLnfos;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getAdid() {
        return adid;
    }

    public void setAdid(int adid) {
        this.adid = adid;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }
}
