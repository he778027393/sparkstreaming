package com.gx.domain;

public class ad_click_trend {
    private String hour;
    private String minute;
    private String data;
    private int adid;
    private int clickCount;

    @Override
    public String toString() {
        return "ad_click_trend{" +
                "hour='" + hour + '\'' +
                ", minute='" + minute + '\'' +
                ", data='" + data + '\'' +
                ", adid=" + adid +
                ", clickCount=" + clickCount +
                '}';
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getAdid() {
        return adid;
    }

    public void setAdid(int adid) {
        this.adid = adid;
    }

    public int getClickCount() {
        return clickCount;
    }

    public void setClickCount(int clickCount) {
        this.clickCount = clickCount;
    }
}
