package com.gx.domain;

public class page_split_convert_rate_0308 {
    private String taskid;
    private String convertRate;

    @Override
    public String toString() {
        return "page_split_convert_rate_0308{" +
                "taskid='" + taskid + '\'' +
                ", convertRate='" + convertRate + '\'' +
                '}';
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public String getConvertRate() {
        return convertRate;
    }

    public void setConvertRate(String convertRate) {
        this.convertRate = convertRate;
    }
}
