package com.gx.domain;

public class ad_user_click_count {
    private String data;
    private int adid;
    private int clickCount;
    private int userid;

    @Override
    public String toString() {
        return "ad_user_click_count{" +
                "data='" + data + '\'' +
                ", adid=" + adid +
                ", clickCount=" + clickCount +
                ", userid=" + userid +
                '}';
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getAdid() {
        return adid;
    }

    public void setAdid(int adid) {
        this.adid = adid;
    }

    public int getClickCount() {
        return clickCount;
    }

    public void setClickCount(int clickCount) {
        this.clickCount = clickCount;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }
}
