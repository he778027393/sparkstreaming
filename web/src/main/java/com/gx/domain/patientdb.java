package com.gx.domain;

public class patientdb {
    private String taskid                ;
    private int visit_count            ;
    private double visit_day_1d_3d_ratio  ;
    private double visit_day_4d_6d_ratio  ;
    private double visit_day_7d_9d_ratio  ;
    private double visit_day_10d_30d_ratio;
    private double visit_day_30d_60d_ratio;
    private double visit_day_60d_ratio     ;
    private double step_day_1_3_radio;

    @Override
    public String toString() {
        return "patientdb{" +
                "taskid='" + taskid + '\'' +
                ", visit_count=" + visit_count +
                ", visit_day_1d_3d_ratio=" + visit_day_1d_3d_ratio +
                ", visit_day_4d_6d_ratio=" + visit_day_4d_6d_ratio +
                ", visit_day_7d_9d_ratio=" + visit_day_7d_9d_ratio +
                ", visit_day_10d_30d_ratio=" + visit_day_10d_30d_ratio +
                ", visit_day_30d_60d_ratio=" + visit_day_30d_60d_ratio +
                ", visit_day_60d_ratio=" + visit_day_60d_ratio +
                ", step_day_1_3_radio=" + step_day_1_3_radio +
                ", step_day_4_6_radio=" + step_day_4_6_radio +
                ", step_day_7_9_radio=" + step_day_7_9_radio +
                ", step_day_10_30_radio=" + step_day_10_30_radio +
                ", step_day_30_radio=" + step_day_30_radio +
                '}';
    }

    public double getStep_day_1_3_radio() {
        return step_day_1_3_radio;
    }

    public void setStep_day_1_3_radio(double step_day_1_3_radio) {
        this.step_day_1_3_radio = step_day_1_3_radio;
    }

    private double step_day_4_6_radio      ;
    private  double step_day_7_9_radio      ;
    private double step_day_10_30_radio     ;
    private double step_day_30_radio        ;

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public int getVisit_count() {
        return visit_count;
    }

    public void setVisit_count(int visit_count) {
        this.visit_count = visit_count;
    }

    public double getVisit_day_1d_3d_ratio() {
        return visit_day_1d_3d_ratio;
    }

    public void setVisit_day_1d_3d_ratio(double visit_day_1d_3d_ratio) {
        this.visit_day_1d_3d_ratio = visit_day_1d_3d_ratio;
    }

    public double getVisit_day_4d_6d_ratio() {
        return visit_day_4d_6d_ratio;
    }

    public void setVisit_day_4d_6d_ratio(double visit_day_4d_6d_ratio) {
        this.visit_day_4d_6d_ratio = visit_day_4d_6d_ratio;
    }

    public double getVisit_day_7d_9d_ratio() {
        return visit_day_7d_9d_ratio;
    }

    public void setVisit_day_7d_9d_ratio(double visit_day_7d_9d_ratio) {
        this.visit_day_7d_9d_ratio = visit_day_7d_9d_ratio;
    }

    public double getVisit_day_10d_30d_ratio() {
        return visit_day_10d_30d_ratio;
    }

    public void setVisit_day_10d_30d_ratio(double visit_day_10d_30d_ratio) {
        this.visit_day_10d_30d_ratio = visit_day_10d_30d_ratio;
    }

    public double getVisit_day_30d_60d_ratio() {
        return visit_day_30d_60d_ratio;
    }

    public void setVisit_day_30d_60d_ratio(double visit_day_30d_60d_ratio) {
        this.visit_day_30d_60d_ratio = visit_day_30d_60d_ratio;
    }

    public double getVisit_day_60d_ratio() {
        return visit_day_60d_ratio;
    }

    public void setVisit_day_60d_ratio(double visit_day_60d_ratio) {
        this.visit_day_60d_ratio = visit_day_60d_ratio;
    }

    public double getStep_day_4_6_radio() {
        return step_day_4_6_radio;
    }

    public void setStep_day_4_6_radio(double step_day_4_6_radio) {
        this.step_day_4_6_radio = step_day_4_6_radio;
    }

    public double getStep_day_7_9_radio() {
        return step_day_7_9_radio;
    }

    public void setStep_day_7_9_radio(double step_day_7_9_radio) {
        this.step_day_7_9_radio = step_day_7_9_radio;
    }

    public double getStep_day_10_30_radio() {
        return step_day_10_30_radio;
    }

    public void setStep_day_10_30_radio(double step_day_10_30_radio) {
        this.step_day_10_30_radio = step_day_10_30_radio;
    }

    public double getStep_day_30_radio() {
        return step_day_30_radio;
    }

    public void setStep_day_30_radio(double step_day_30_radio) {
        this.step_day_30_radio = step_day_30_radio;
    }
}
