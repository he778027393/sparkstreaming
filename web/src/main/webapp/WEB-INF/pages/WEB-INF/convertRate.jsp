
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
    <script  src="${pageContext.request.contextPath}/statics/js/echarts.js"></script>
</head>
<body style="background-color:#6c757d">
<br>

<div id="main" style="width: 1500px;height: 700px"></div>

<script type="text/javascript">

    var list = ${convertRate}
    console.log(list)
    var myChart=echarts.init(document.getElementById('main'));
    var option={
        title:{
            text:'页面转化率',
            left:'25%',
            textStyle:{
                fontSize:25
            }},
        tooltip:{},
        legend: {
                data:['时间'],
                bottom:'15',
                right:'15'
            },
        radar :{
            name:{
                textStyle: {
                    padding:[2,3],
                    fontSize: 12
                }
            },
            indicator:[{text : '页面2转化率', max  : 1.5},
                {text : '页面3转化率', max  : 1.3},
                {text : '页面4转化率', max  : 1.5},
                {text : '页面5转化率', max  : 1.5},
                {text : '页面6转化率', max  : 1.2},],
            nameGap:4,
            splitNumber:4,
            axisLine:{
                show:true,
                symbol:"circle",
                symbolSize:[4,4]
            }

        },
        series:[{
            name:'time',
            type:'radar',
            symbol: 'circle',
            symbolSize: 4,
            data:[
                {
                    value:list,
                    name:'time',
                    itemStyle:{
                        color:'red'
                    }
                }
            ]
        }]
    };

    myChart.setOption(option);
</script>

<br>
<br>
<br>



</body>
</html>
