<%--
  Created by IntelliJ IDEA.
  User: 宜春
  Date: 2019/9/2
  Time: 7:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>

<html>
<head>
    <title>Title</title>
    <script  src="${pageContext.request.contextPath}/statics/js/echarts.js"></script>
</head>
<body>
<br>

<div id="main" style="width: 1500px;height: 700px"></div>

<script type="text/javascript">

    var list = ${categoryid}
    var list1 = ${clickCount}
    console.log(list)
    console.log(list1)
    var myChart = echarts.init(document.getElementById('main'));
    var option = {
        title: {text:'Top10会话'},
        tooltip: {},
        legend: {
            data:['会话']
        },
        grid: {
            left: 300
        },
        xAxis: {
            type: 'category',
            data: list
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            name: '数量',
            data: list1,
            type: 'line'
        }]

    };
    myChart.setOption(option);
</script>

<br>
<br>
<br>



</body>
</html>
