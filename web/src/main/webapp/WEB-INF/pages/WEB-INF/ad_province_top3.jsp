<%--
  Created by IntelliJ IDEA.
  User: 杨君胜
  Date: 2021/8/15
  Time: 20:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <script src="${pageContext.request.contextPath}/statics/js/echarts.js"></script>
</head>
<body>
<br>
<div id="main" style="width: 1500px;height: 700px">
</div>
<script type="text/javascript">
    var seriesLabel = {
        normal: {
            show: true,
            textBorderColor: '#333',
            textBorderWidth: 1
        }
    }
    var list =${province}
    var list1 = ${clickCount}
    var list3=${ids}
    var list4=new Array()
    var list5=new Array()
    var list6=new Array()
    for(var i=0;i<30;i+=3)
    {
        list4.push(list3[i])
    }
    for(var i=1;i<30;i+=3)
    {
        list5.push(list3[i])
    }
    for(var i=2;i<30;i+=3)
    {
        list6.push(list3[i])
    }
    var option = {
        title: {
            text: 'Top3各省广告'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: ['Ad1', 'Ad2', 'Ad3']
        },
        grid: {
            left: 300
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            inverse: true,
            data: list,
            axisLabel: {
                margin: 20,
            },
            name:'省份编号'

        },
        yAxis: {
            type: 'value',
            name: 'Count',
            axisLabel: {
                formatter: '{value}'
            }

        },
        series: [
            {
                name: 'Ad1',
                type: 'bar',
                data: list4,
                label: seriesLabel
            },
            {
                name: 'Ad2',
                type: 'bar',
                label: seriesLabel,
                data: list5
            },
            {
                name: 'Ad3',
                type: 'bar',
                label: seriesLabel,
                data: list6
            }
        ]
    };





        console.log(list)
    console.log(list1)
    console.log(list3)
    console.log(list4)
    console.log(list5)
    console.log(list6)
    var myChart = echarts.init(document.getElementById('main'));
    var option1 = {
        title: {text: 'Top3省份广告'},
        tooltip: {},
        legend: {
            data: ['排行榜']
        },
        xAxis: {
            data: list
        },
        yAxis: {},
        series: [{
            name: list3,
            type: 'bar',
            data: list1
        }]

    };
    myChart.setOption(option);
</script>

</body>
</html>
