<%--
  Created by IntelliJ IDEA.
  User: 杨君胜
  Date: 2021/8/15
  Time: 20:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <script src="${pageContext.request.contextPath}/statics/js/echarts.js"></script>
</head>
<body>
<br>
<div id="main" style="width: 1500px;height: 700px"></div>
<script type="text/javascript">

    var list =${Categoryid}
    var list1 = ${clickCount}
        console.log(list)
    console.log(list1)
    var myChart = echarts.init(document.getElementById('main'));
    var option = {
        title: {text: 'Top10热门品种'},
        tooltip: {},
        legend: {
            data: ['排行榜']
        },
        grid: {
            left: 300
        },
        xAxis: {
            data: list
        },
        yAxis: {},
        series: [{
            name: '数量',
            type: 'bar',
            data: list1
        }]

    };
    myChart.setOption(option);
</script>

</body>
</html>
