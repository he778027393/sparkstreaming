
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
    <script  src="${pageContext.request.contextPath}/statics/js/echarts.js"></script>
</head>
<body style="background-color:#cfd2df">
<br>

<div id="main" style="width: 1500px;height: 700px"></div>

<script type="text/javascript">

    var list = ${userid}
        console.log(list)
    var list2 = ${clickCount}
        console.log(list2)
    var sum1=0
    var sum2=0
    var sum3=0
    for(var i=0;i<list2.length;i+=3)
    {
        sum1+=list2[i];
    }
    for(var i=1;i<list2.length;i+=3)
    {
        sum2+=list2[i];
    }
    for(var i=2;i<list2.length;i+=3)
    {
        sum3+=list2[i];
    }
    sum1/=(sum1+sum2+sum3)
    sum2/=(sum1+sum2+sum3)
    sum3=1-sum1-sum2
    console.log(sum1)
    console.log(sum2)
    console.log(sum3)

    var myChart=echarts.init(document.getElementById('main'));
    var option={
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c}%'
        },
        legend: {
            orient: 'vertical',
            left: 10,
            data: ['用户群体1', '用户群体2', '用户群体3']
        },
        color: ['#1689fe', '#cd9d9d', '#8c79fe', '#90c5ed', '#bfc078'], // 自定义颜色
        series: [
            {
                name: '占比',
                type: 'pie',
                minAngle: 10, // 最小的扇区角度（0 ~ 360），用于防止某个值过小导致扇区太小影响交互
                radius: ['30%', '70%'],
                center: ['48%', '58%'],
                label: {
                    formatter: '{b|{b}}\n（{c}）\n', // 这里的设置就是饼图的标签内容及其格式
                    rich: {
                        b: {
                            align: 'center'
                        }
                    }
                },
                data: [
                    {value: sum1, name: '用户群体1'},
                    {value: sum2, name: '用户群体2'},
                    {value: sum3, name: '用户群体3'},
                ]
            }
        ]
};

    myChart.setOption(option);
</script>

<br>
<br>
<br>



</body>
</html>
